import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;

/**
 * Created by OEM on 2017-11-01.
 */
public class AgentProducer extends Agent {

    @Override
    protected void setup() {
        super.setup();
        System.out.println("AgentProducer działą");

        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("token-sender");
        sd.setName("producer");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }

     // tworzenie obiektu klasy CyclicBehaviour -> tworzenie i dodawanie tokenu
        SendingTokenBehaviour sendToken = new SendingTokenBehaviour();
        sendToken.setTokenUtils(new TokenUtils());
        addBehaviour(sendToken);

        /** Ustawia maksymalna ilosc tokenow */
        sendToken.getTokenUtils().setMax(15);

        /** Zachowanie wzbudzajace sie co okreslony czas, powodujace dodanie tokenu */
        Behaviour tickerBehaviour = new TickerBehaviour(this, 500) {
            protected void onTick() {
                /** Geneeruje i dodaje token az do osiagniecia maxa*/
                if (sendToken.getTokenUtils().getCount() < sendToken.getTokenUtils().getMax()) {

                    sendToken.getTokenUtils().createToken();
                    sendToken.getTokenUtils().setCount(sendToken.getTokenUtils().getCount() + 1);
                    System.out.println("$ Token nr: " + sendToken.getTokenUtils().getCount() + "/"
                            + sendToken.getTokenUtils().getMax());
                }
            }
        };
        addBehaviour(tickerBehaviour);
    }
}



