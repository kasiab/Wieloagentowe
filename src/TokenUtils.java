import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by OEM on 2017-11-02.
 */
public class TokenUtils {

    private List<Token> list = new ArrayList<>();
    private Integer max = 0;
    private Integer count = 0;

    public void createToken()
    {
        if(count <= max) {
            Token token = new Token();
            token.setToken(UUID.randomUUID().toString());
            list.add(token);
        }
    }

    /**
     * Wartosc true jesli uda sie usunac, false jesli wystapi wyjatek i token nie zostanie usuniety z listy
     */
    public Token getTokenFromList()
    {
        Token token = new Token();
        if(!list.isEmpty()) {
            token = list.get(0);
            list.remove(0);
        }
        return token;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
