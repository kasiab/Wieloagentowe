import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

/**
 * Created by OEM on 2017-11-02.
 */
public class SendingTokenBehaviour extends CyclicBehaviour {

    TokenUtils tokenUtils = new TokenUtils();

    public void action() {

        /** Server nasluchuje wiadomosci typu request*/
        MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
        ACLMessage msg = myAgent.receive(mt);

        if (msg != null) {
            ACLMessage reply = msg.createReply();

            //getting Token from created list
            Token token = tokenUtils.getTokenFromList();

            if(token.getToken() != null)
            {
                reply.setContent(token.getToken());
                System.out.println("AgentProducer: Wysylam token do: " + msg.getSender().getName());
            } else if(tokenUtils.getCount() == tokenUtils.getMax()){
                /** Wygenerowano i zuzyto wszystkie tokeny*/
                reply.setPerformative(ACLMessage.CANCEL);
            } else{
                /** Nie nadaza z generowaniem tokenow*/
                reply.setPerformative(ACLMessage.FAILURE);
            }
            myAgent.send(reply);
        }
        else{
            block();
        }
    }

    public TokenUtils getTokenUtils() {
        return tokenUtils;
    }

    public void setTokenUtils(TokenUtils tokenUtils) {
        this.tokenUtils = tokenUtils;
    }
}
