import jade.core.*;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;

public class AgentConsumer extends Agent {

    /** Lista serwerow*/
    private AID[] producer;
    /** Ustawia zmienna wspomagajaca oczekiwanie dana ilosc cykli*/
    private Integer licz = 0;
    private ACLMessage message;
    int howManyReceivedMessages = 0;
    private String name;

    @Override
    protected void setup() {
        super.setup();
        long time = 1000;
        /** Dodaje cykliczne zachowanie*/
        addBehaviour(new TickerBehaviour(this, time) {
            protected void onTick() {
              if(licz > 0){
                    licz = licz -1;
                }
                if(licz == 0) {
                    AddProducerAgents(myAgent);
                    message = myAgent.receive();
                    if (message == null) {
                        message = new ACLMessage(ACLMessage.REQUEST);
                        message.addReceiver(producer[0]);
                        myAgent.send(message);
                        block();
                    } else {
                        /** otrzymal token*/
                        if (message.getPerformative() == ACLMessage.REQUEST) {
                            //wypisuje odebrana wiadomosc
                            System.out.println("AgentConsumer otrzymał token: " + message.getContent());
                            howManyReceivedMessages++;
                            licz = 2;
                            /** nie ma tokenow, ale nie osiagnieto maksymalnej ilosci*/
                        } else if (message.getPerformative() == ACLMessage.FAILURE) {
                            licz = 2;

                            /** osiagnieto maksymalna ilosc tokenow*/
                        } else if (message.getPerformative() == ACLMessage.CANCEL) {
                            System.out.println("# " + myAgent.getName() + " odebral: " + howManyReceivedMessages);
                            name = myAgent.getName();
                            doDelete();
                        }
                    }
                }
            }
        });
    }

    /** Poszukuje agenta servera o zadanym typie i nazwie z ktorym bedzie sie laczyc*/
    private void AddProducerAgents(Agent myAgent){
        try {
            DFAgentDescription dfAgentDescription = new DFAgentDescription();
            ServiceDescription serviceDescription = new ServiceDescription();

            /** poszukuje serwera o podanym typie i zazwie*/
            serviceDescription.setType("token-sender");
            serviceDescription.setName("producer");
            dfAgentDescription.addServices(serviceDescription);
            /** pobiera liste agentow spelniajacych warunek*/
            DFAgentDescription[] result = DFService.search(myAgent, dfAgentDescription);

            producer = new AID[result.length];
            for (int i = 0; i < result.length; i++) {
                producer[i] = result[i].getName();
            }
        } catch (FIPAException exception) {
            exception.printStackTrace();
        }
    }
}
